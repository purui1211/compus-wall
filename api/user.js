import request from '@/common/request.js';


export function login(username,pwd) {
	const opts = {
		url: '/auth/auth/login',
		method: 'post',
		data: {
			"username":username,
			"password":pwd
		}
	}
  return request.httpRequest(opts)
}

export function register(data) {
	const opts = {
		url: '/content/user/register',
		method: 'post',
		data: data
	}
  return request.httpRequest(opts)
}

export function getToken(data) {
	const opts = {
		url: '/auth/auth/getInfo',
		method: 'post',
		data: {
			"token":data
		}
	}
  return request.httpTokenRequest(opts)
}

// 获取用户id
export function getLoginId(data) {
	const opts = {
		url: '/auth/auth/getLoginUserId/'+data,
		method: 'post'
	}
  return request.httpTokenRequest(opts)
}
// 获取用户主页信息
export function getHomePage() {
	const opts = {
		url: '/content/user/getHomePage',
		method: 'post'
	}
  return request.httpTokenRequest(opts)
}

