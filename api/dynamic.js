import request from '@/common/request.js';

export function issue(content,uri) {
	const opts = {
		url: '/content/dynamic/issue',
		method: 'post',
		data: {
			"content":content,
			"url":uri
		}
	}
	console.log(opts)
  return request.httpTokenRequest(opts)
}

export function queryList(content) {
	console.log(content)
	const opts = {
		url: '/content/dynamic/query',
		method: 'post',
		data: {
			"content":""+content+""
		}
	}
  return request.httpRequest(opts)
}
export function queryById() {
	const opts = {
		url: '/content/dynamic/queryById',
		method: 'post',
		data:''
	}
  return request.httpTokenRequest(opts)
}