import request from '@/common/request.js';


export function success() {
	const opts = {
		url: '/test/test/success',
		method: 'get',
		data: null
	}
  return request.httpRequest(opts)
}

export function select() {
	const opts = {
		url: '/test/test/select',
		method: 'get',
		data: null
	}
  return request.httpRequest(opts)
}

export function selectnews() {
	const opts = {
		url: '/test/test/news',
		method: 'get',
		data: null
	}
  return request.httpRequest(opts)
}
