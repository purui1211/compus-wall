import request from '@/common/request.js';
export function att() {
	const opts = {
		url: '/content/connect/att',
		method: 'post',
		// data: ""
	}
  return request.httpTokenRequest(opts)
}

export function fans() {
	const opts = {
		url: '/content/connect/fans',
		method: 'post',
		// data: ""
	}
  return request.httpTokenRequest(opts)
}

export function add(data) {
	const opts = {
		url: '/content/connect/add/'+data,
		method: 'post',
		// data: ""
	}
  return request.httpTokenRequest(opts)
}

export function del(data) {
	const opts = {
		url: '/content/connect/delete/'+data,
		method: 'post',
		// data: ""
	}
  return request.httpTokenRequest(opts)
}
