import request from '@/common/request.js';

export function release(title,type,content,phone,uri) {
	const opts = {
		url: '/content/demand/release',
		method: 'post',
		data:{
			"title":title,
			"content":content,
			"type":type,
			"phone":phone,
			"url":uri
		}
	}
	console.log(opts)
  return request.httpTokenRequest(opts)
}
export function queryList(content) {
	console.log(content)
	const opts = {
		url: '/content/demand/query',
		method: 'post',
		data: {
			"content":""+content+""
		}
	}
  return request.httpRequest(opts)
}