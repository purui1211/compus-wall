// request.js

// 封装网络请求函数
export const request = (options) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: options.url,
			method: options.method || 'GET',
			data: options.data || {},
			header: options.headers || {},
			success: (res) => {
				resolve(res.data);
			},
			fail: (error) => {
				reject(error);
			},
		});
	});
};