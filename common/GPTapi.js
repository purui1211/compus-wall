// api.js

import axios from 'axios';

// 定义 ChartGPT 3.5 文字接口请求函数
export const generateChart = async (message) => {
  try {
    const response = await axios.post('https://api.openai.com/v1/engines/davinci-codex/completions', {
      prompt: message,
      max_tokens: 50,
      temperature: 0.7,
    }, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer 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', // 替换为你的 ChartGPT 3.5 API 密钥
      },
    });

    return response.data.choices[0].text.trim();
  } catch (error) {
	  console.log(error.message)
    // throw new Error('生成图表失败，请重试。');
  }
};